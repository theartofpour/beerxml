beerxml
=======

Go module implements a way to read BeerXML files, It aims to cover most of the variations

![img](http://i.imgur.com/IMUKRWQ.jpg)


Reference: http://www.beerxml.com/beerxml.htm


Go package to read BeerXML files designed for the exchange of beer brewing recipes and other brewing data.
